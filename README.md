# Impresive Postres Web

Impresive Postres Web is a website where you can order your own personalizated cakes adding your favorite picture.

![alt](impresivepostres/dist/public/images/create.png)

## Test Users

- **Usuario Prueba 1:**  
  - Email: *gaxiki1719@temhuv.com*
  - Password: Usuario1
- **Usuario Prueba 2:**
  - Email: *bewed71891@vewku.com*
  - Password: Usuario2
- **Usuario Prueba 3:**
  - Email: *xafoc96298@unomail9.com*
  - Password: Usuario3
- **Usuario Prueba 4:**
  - Email: *fipisom133@zkeiw.com*
  - Password: Usuario4

## Author

- **Michel Aldair Perez Reyes**

## Acknowledgments

- KAVAK project 2
- Impresive Postres Local
