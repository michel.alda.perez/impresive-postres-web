import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	OneToMany,
	Generated,
	JoinColumn,
} from 'typeorm';
import { Order } from './Order.entity';

export type UserRole = 'owner' | 'client';

@Entity()
export class User {
	@PrimaryGeneratedColumn()
	id: string;
	@Column()
	username: string;
	@Column()
	email: string;
	@Column({ nullable: true })
	password: string;
	@Column({
		type: 'enum',
		enum: ['owner', 'client'],
		default: 'client',
	})
	role: UserRole;
	@Column({
		default:
			'https://www.dts.edu/wp-content/uploads/sites/6/2018/04/Blank-Profile-Picture.jpg',
	})
	imgUrl: string;
	@Column({ default: false })
	status: boolean;
	@Column({ nullable: true })
	confirmationCode: string;
	@Column({ nullable: true })
	googleId: string;
	@Column({ nullable: true })
	facebookId: string;

	@OneToMany((type) => Order, (order) => order.user)
	@JoinColumn({name: 'order_id'})
	order: Order[];
}
