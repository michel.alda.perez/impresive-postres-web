import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	ManyToOne,
	OneToMany,
	JoinColumn,
} from 'typeorm';
import { User } from './User.entity';
import { Cake } from './Cake.entity';

export type OrderStatus = 'Pendiente' | 'Horneando' | 'Listo';

@Entity()
export class Order {
	@PrimaryGeneratedColumn()
	id: string;

	@CreateDateColumn()
	date: Date;

	@Column({
		type: 'enum',
		enum: ['Pendiente', 'Horneando', 'Listo'],
		default: 'Pendiente',
	})
	status: OrderStatus;

	@ManyToOne((type) => User, (user) => user.order, { onDelete: 'CASCADE' })
	@JoinColumn({ name: 'user_id' })
	user: User;

	@ManyToOne((type) => Cake, (cake) => cake.order, { onDelete: 'CASCADE' })
	@JoinColumn({ name: 'cake_id' })
	cake: Cake;
}
