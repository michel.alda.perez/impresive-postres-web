import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	ManyToOne,
	OneToMany,
	JoinColumn,
} from 'typeorm';
import { Order } from './Order.entity';

export type CakeType = 'Circular' | 'Rectangular';
export type CakeSize = 'Chico' | 'Mediano' | 'Grande';
export type CakeFlavour =
	| 'Chocolate'
	| 'Nuez'
	| 'Naranja'
	| 'Coco'
	| 'Vainilla'
	| 'TresLeches'
	| 'Envinado'
	| 'Seco';
export type CakeFilling =
	| 'MezclaDeFrutas'
	| 'Pinia'
	| 'Mango'
	| 'Fresa'
	| 'Durazno'
	| 'Pera'
	| 'NuezPicada'
	| 'Chantilly'
	| 'CremaPastelera'
	| 'Mermelada'
	| 'Nutella'
	| 'Ninguno';

export type CakeIcing = 'Vainilla' | 'Chocolate';

export type CakeTopping = 'Frase' | 'Fondant' | 'Chantilly' | 'Oblea';

@Entity()
export class Cake {
	@PrimaryGeneratedColumn()
	id: string;

	@Column({ default: 'Mi pastel' })
	name: string;

	@Column({
		type: 'enum',
		enum: ['Circular', 'Rectangular'],
		default: 'Circular',
	})
	type: CakeType;

	@Column({
		type: 'enum',
		enum: ['Chico', 'Mediano', 'Grande'],
		default: 'Chico',
	})
	size: CakeSize;

	@Column({
		type: 'enum',
		enum: [
			'Chocolate',
			'Nuez',
			'Naranja',
			'Coco',
			'Vainilla',
			'TresLeches',
			'Envinado',
			'Seco',
		],
		default: 'Seco',
	})
	flavour: CakeFlavour;

	@Column({
		type: 'enum',
		enum: [
			'MezclaDeFrutas',
			'Pinia',
			'Mango',
			'Fresa',
			'Durazno',
			'Pera',
			'NuezPicada',
			'Chantilly',
			'CremaPastelera',
			'Mermelada',
			'Nutella',
			'Ninguno',
		],
		default: 'Chantilly',
	})
	filling: CakeFilling;

	@Column({
		type: 'enum',
		enum: ['Vainilla', 'Chocolate'],
		default: 'Vainilla',
	})
	icing: CakeIcing;

	@Column({
		type: 'enum',
		enum: ['Frase', 'Fondant', 'Chantilly', 'Oblea'],
		default: 'Oblea',
	})
	topping: CakeTopping;

	@Column({
		default:
			'https://res.cloudinary.com/dbc1hdxqi/image/upload/v1591994110/impresivePostresWeb/impresive-postres-logo_kole6d.png',
	})
	imgUrl: string;

	@Column({ default: 200 })
	cost: number;

	@Column()
	date: Date;

	// @Column('simple-array')
	// direction: string[];

	@OneToMany((type) => Order, (order) => order.cake)
	@JoinColumn({ name: 'order_id' })
	order: Order[];
}
