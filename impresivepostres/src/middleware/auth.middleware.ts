import { Request, Response, NextFunction } from 'express';

export const isLoggedIn = (req: Request, res: Response, next: NextFunction) => {
	req.isAuthenticated() ? next() : res.redirect('/auth/login');
};

export const isActive = (req: any, res: Response, next: NextFunction) => {
	req.user.status
		? next()
		: res.render('auth/login', { message: 'Verifica tu cuenta' });
};

export const authGlobal = (req: any, res: Response, next: NextFunction) => {
	if (req.isAuthenticated()) {
		req.app.locals.logged = true;
	} else {
		req.app.locals.logged = false;
	}
	next();
};
