import { Router } from 'express';
import uploadCloud from '../config/cloudinary.config';

import {
	getCreate,
	postCreate,
	getCakeDetails,
	getCakeDelete,
	postCakeUpdate,
} from '../controllers/cake.controller';
import { isLoggedIn, isActive } from '../middleware/auth.middleware';

const router = Router();

// getCreate route check if logged 
router.get('/create', isLoggedIn, isActive, getCreate);
// post Create check if logged and use cloudinary to upload imgUrl
router.post(
	'/create',
	isLoggedIn,
	isActive,
	uploadCloud.single('imgUrl'),
	postCreate
);

// getDetails check if logged
router.get('/:cakeId/details', isLoggedIn, isActive, getCakeDetails);
// postUpdate check if logged and use cloudinary to upload imgurl
router.post(
	'/:cakeId/update',
	isLoggedIn,
	isActive,
	uploadCloud.single('imgUrl'),
	postCakeUpdate
);

// getDelete check if logged 
router.get('/:cakeId/delete', isLoggedIn, isActive, getCakeDelete);

export default router;
