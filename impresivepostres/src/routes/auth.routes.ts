import { Router } from 'express';
import passport from 'passport';
import uploadCloud from '../config/cloudinary.config';

import {
	getSignup,
	postSignup,
	getLogin,
	postLogin,
	logout,
	getConfirm,
} from '../controllers/auth.controller';
import { isLoggedIn, isActive } from '../middleware/auth.middleware';

const router = Router();

router.get('/signup', getSignup);
router.post('/signup', postSignup);
router.get('/login', getLogin);
router.post('/login', postLogin);
router.get('/logout', isLoggedIn, isActive, logout);

router.get('/confirm/:confirmationCode', getConfirm);

router.get(
	'/google',
	passport.authenticate('google', {
		scope: [
			'https://www.googleapis.com/auth/userinfo.profile',
			'https://www.googleapis.com/auth/userinfo.email',
		],
	})
);

router.get(
	'/google/callback',
	passport.authenticate('google', {
		successRedirect: '/user/profile',
		failureRedirect: '/auth/login',
	})
);

router.get(
	'/facebook',
	passport.authenticate('facebook', {
		scope: ['email'],
	})
);

router.get(
	'/facebook/callback',
	passport.authenticate('facebook', {
		successRedirect: '/user/profile',
		failureRedirect: '/auth/login',
	})
);

export default router;
