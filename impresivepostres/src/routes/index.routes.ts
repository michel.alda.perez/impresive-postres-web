import { Router, Request, Response, NextFunction } from 'express';

const router = Router();

router.get('/', (req: Request, res: Response, next: NextFunction ): void => {
  res.render('index');
});

router.get('/about', (req: Request, res: Response, next: NextFunction ): void => {
  res.render('about');
});

export default router;