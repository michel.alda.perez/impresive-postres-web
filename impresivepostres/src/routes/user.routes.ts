import { Router } from 'express';
import passport from 'passport';
import uploadCloud from '../config/cloudinary.config';
import { isLoggedIn, isActive } from '../middleware/auth.middleware';
import { getProfile, postProfile } from '../controllers/user.controller';

const router = Router();

router.get('/profile', isLoggedIn, isActive, getProfile);
router.post(
	'/:userId/update',
	isLoggedIn,
	isActive,
	uploadCloud.single('imgUrl'),
	postProfile
);

export default router;
