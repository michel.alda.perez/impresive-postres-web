import fs from 'fs';
import path from 'path';
import hbs from 'hbs';
import nodemailer from 'nodemailer';

const verficationTemplate = hbs.compile(
	fs.readFileSync(path.join(__dirname, '../views/auth/confirm.hbs'), 'utf8')
);

const transporter = nodemailer.createTransport({
	host: 'smtp.gmail.com',
	service: 'gmail',
	auth: {
		user: process.env.EMAIL,
		pass: process.env.EMAIL_PASSWORD,
	},
});

export const confirmAccount = async (to: string, confirmationCode: string) => {
	return await transporter.sendMail({
		from: 'Impresive Postres <impresivepostres@gmail.com>',
		to,
		subject: 'Confirma tu cuenta - Impresive Postres',
		html: verficationTemplate({ confirmationCode }),
	});
};
