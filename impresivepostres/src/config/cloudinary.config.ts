const cloudinary = require('cloudinary').v2;
import { CloudinaryStorage } from 'multer-storage-cloudinary';
import multer from 'multer';

cloudinary.config({
	cloud_name: process.env.CLOUDINARY_NAME,
	api_key: process.env.CLOUDINARY_KEY,
	api_secret: process.env.CLOUDINARY_SECRET,
});

const storage = new CloudinaryStorage({
	cloudinary,
	params: {
		folder: 'impresivePostresWeb/images',
		allowed_formats: ['jpg', 'jpeg', 'png', 'gif'],
	},
});

const uploadCloud = multer({ storage });

export default uploadCloud;
