import passport from 'passport';
const LocalStrategy = require('passport-local').Strategy;
const googleStrategy = require('passport-google-oauth20').Strategy;
const facebookStrategy = require('passport-facebook').Strategy;
import bcrypt from 'bcrypt';
import { User } from '../entities/User.entity';
import { getRepository } from 'typeorm';

const verifyCallback = async (email, password, done) => {
	try {
		const user = await getRepository(User).findOne({ email: email });
		if (!user) {
			return done(null, false, {
				message: 'El correo ingresado no está registrado',
			});
		}
		if (!user.password) {
			return done(null, false, {
				message:
					'Te registraste usando una de tus redes sociales. Inicia mediante una de ellas.',
			});
		}
		const isValid = bcrypt.compareSync(password, user.password);

		if (isValid) return done(null, user);
		else return done(null, false, { message: 'Contraseña incorrecta' });
	} catch (err) {
		done('There was an error with the server', false, {
			message: 'Server internal error',
		});
		console.log(err);
	}
};

const strategy = new LocalStrategy(
	{
		usernameField: 'email',
		passwordField: 'password',
	},
	verifyCallback
);

const googleVerifyCallback = async (_, __, profile, done) => {
	const user = await getRepository(User)
		.findOne({ googleId: profile.id })
		.catch((error) => done(error));
	if (user) {
		user.profilePhotoUrl = profile._json.picture;
		await getRepository(User).save(user);
		done(null, user);
	} else {
		const newUser = getRepository(User).create({
			googleId: profile.id,
			username: profile.displayName,
			email: profile._json.email,
			imgUrl: profile._json.picture,
			status: true,
		});
		await getRepository(User).save(newUser);
		done(null, newUser);
	}
};

const gStrategy = new googleStrategy(
	{
		clientID: process.env.GOOGLE_ID,
		clientSecret: process.env.GOOGLE_SECRET,
		callbackURL: `${process.env.ENDPOINT}/auth/google/callback`,
	},
	googleVerifyCallback
);

const facebookVerifyCallback = async (_, __, profile, done) => {
	console.log('Facebook Profile: ', profile);
	// console.log('Imagen Facebook', profile._json.picture)
	// console.log('Imagen Facebook', profile.picture.data)

	const user = await getRepository(User)
		.findOne({ facebookId: profile.id })
		.catch((err) => done(err));
	if (user) {
		user.profilePhotoUrl = profile.photos[0].value;
		await getRepository(User).save(user);
		done(null, user);
	} else {
		const newUser = getRepository(User).create({
			facebookId: profile.id,
			username: profile.displayName,
			email: profile.emails[0].value,
			imgUrl: profile.photos[0].value,
			status: true,
		});
		await getRepository(User).save(newUser);
		console.log('New facebook user: ', newUser);
		done(null, newUser);
	}
};

const fStrategy = new facebookStrategy(
	{
		clientID: process.env.FACEBOOK_ID,
		clientSecret: process.env.FACEBOOK_SECRET,
		callbackURL: `${process.env.ENDPOINT}/auth/facebook/callback`,
		profileFields: ['id', 'displayName', 'photos', 'emails', 'profileUrl'],
	},
	facebookVerifyCallback
);

passport.use(fStrategy);

passport.use(gStrategy);

passport.use(strategy);

passport.serializeUser((user: User, cb) => {
	cb(null, user.id);
});

passport.deserializeUser((id, cb) => {
	getRepository(User)
		.findOne(id)
		.then((user) => cb(null, user))
		.catch((error) => cb(error, null));
});

export default passport;
