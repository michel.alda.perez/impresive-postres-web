import { Request, Response, NextFunction } from 'express';
import { User } from '../entities/User.entity';
import { getRepository } from 'typeorm';
import passport from '../config/passport.config';
import bcrypt from 'bcrypt';
import { confirmAccount } from '../config/nodemailer.config';

//SIGNUP
export const getSignup = (req: Request, res: Response, next: NextFunction) => {
	res.render('auth/signup');
};

export const postSignup = (req: Request, res: Response, next: NextFunction) => {
	const { email, username, password, checkpass } = req.body;

	const user = {
		username,
		email,
		password,
		checkpass,
	};

	if (email === '' || password === '' || username === '') {
		res.render('auth/signup', {
			message: 'Faltan campos por llenar',
			user,
		});
	} else if (!validateEmail(email)) {
		res.render('auth/signup', {
			message: 'Dirección de correo no válida',
			user,
		});
	} else if (!validatePassword(password)) {
		res.render('auth/signup', {
			message:
				'La contraseña debe tener al menos 8 caracteres, una letra minúscula y una mayúscula',
			user,
		});
	} else if (checkpass !== password) {
		res.render('auth/signup', {
			message: 'Las contraseñas no coinciden',
			user,
		});
	} else {
		let randomCode = generateRandomToken();
		getRepository(User)
			.findOne({ email })
			.then((user) => {
				if (!user) {
					const salt = bcrypt.genSaltSync(10);
					const hashPassword = bcrypt.hashSync(password, salt);
					const newUser = getRepository(User).create({
						email,
						username,
						password: hashPassword,
						confirmationCode: randomCode,
					});					
					getRepository(User)
						.save(newUser)
						.then(async (usr) => {							
							await confirmAccount(
								email,
								`${process.env.ENDPOINT}/auth/confirm/${usr.confirmationCode}`
							);
							res.redirect('/auth/login');
						})
						.catch((err) => console.log(err));
				} else {
					res.render('auth/signup', {
						message: 'Esta dirección de correo ya esta en uso',
					});
				}
			})
			.catch((err) => console.log(err));
	}
};

//LOGIN
export const getLogin = (req: any, res: Response, next: NextFunction) => {
	let message = req.flash('error')[0];
	res.render('auth/login', { message });
};

export const postLogin = passport.authenticate('local', {
	successRedirect: '/user/profile',
	failureRedirect: '/auth/login',
	failureFlash: true,
	
});

//LOGOUT
export const logout = (req: any, res: Response, next: NextFunction) => {
	req.session.destroy(() => {
		req.logOut();
		res.clearCookie('graphNodeCookie');
		res.status(200);
		res.redirect('/auth/login');
	});
};

export const getConfirm = async (
	req: any,
	res: Response,
	next: NextFunction
) => {
	const { confirmationCode } = req.params;
	try {
		const userConfirm = await getRepository(User).findOne({
			confirmationCode,
		});
		await getRepository(User).update(userConfirm.id, { status: true });
		res.redirect('/user/profile');
	} catch (error) {
		console.log('getConfirm error: ', error);
	}
};

//AUXILIAR FUNCTIONS
/**
 * Function to check email structure
 * @param email string
 */
const validateEmail = (email) => {
	const emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	return email.match(emailFormat) ? true : false;
};

/**
 * Function to check password structure (lenght 8, number, uppercase)
 * @param password 
 */
const validatePassword = (password) => {
	const passwordFormat = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;
	return password.match(passwordFormat) ? true : false;
};

/**
 * Function to generate random token for email verification
 */
const generateRandomToken = () => {
	const characters =
		'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	let token = '';
	for (let i = 0; i < 25; i++) {
		token += characters[Math.floor(Math.random() * characters.length)];
	}
	return token;
};
