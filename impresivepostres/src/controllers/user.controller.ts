import { Request, Response, NextFunction } from 'express';
import { Cake } from '../entities/Cake.entity';
import { getRepository } from 'typeorm';
import { Order } from '../entities/Order.entity';
import { User } from '../entities/User.entity';

export const getProfile = async (
	req: Request,
	res: Response,
	next: NextFunction
) => {
	const { user } = req;
	const userID = user['id'];
	console.log('getProfile USER: ', user);
	console.log('getProfile USERID: ', userID);
	if (user['role'] === 'owner') {
		try {
			const owner = true;
			const cakes = await getRepository(Cake)
				.createQueryBuilder('cake')
				.innerJoinAndSelect('cake.order', 'order')
				.execute();
			console.log('getProfile  ADMIN USERCAKES: ', cakes);
			res.render('user/profile', { user, cakes, owner });
		} catch (error) {
			console.log(error);
		}
	} else {
		try {
			const cakes = await getRepository(Cake)
				.createQueryBuilder('cake')
				.innerJoinAndSelect('cake.order', 'order')
				.where('order.user_id = :user_id', { user_id: userID })
				.execute();
			console.log('getProfile USERCAKES: ', cakes);
			res.render('user/profile', { user, cakes });
		} catch (error) {
			console.log(error);
		}
	}
};

export const postProfile = async (
	req: any,
	res: Response,
	next: NextFunction
) => {
	try {
		const { id } = req.user;
		if (req.file !== undefined) {
			const { path } = req.file;
			try {
				const user = await getRepository(User).update(id, {
					imgUrl: path,
				});
				req.user = user;
				res.redirect('/user/profile');
			} catch (error) {
				console.log('Image not selected');
				res.render('user/profile', { message: 'No seleccionaste una imagen' });
			}
		} else {
			res.redirect('/user/profile');
		}
	} catch (error) {
		console.log('not path');
		res.redirect('/auth/profile');
	}
};
