import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';
import { Order } from '../entities/Order.entity';
import { Cake } from '../entities/Cake.entity';
/**
 * Function to getCreate
 * @param req Request
 * @param res Response
 * @param next NextFunction
 */
export const getCreate = (req: Request, res: Response, next: NextFunction) => {
	res.render('cake/create');
};

/**
 * Function to create cake using method POST
 * @param req Any
 * @param res Response
 * @param next Nextfunction
 */
export const postCreate = async (
	req: any,
	res: Response,
	next: NextFunction
) => {
	const { user } = req;
	const userID = user['id'];
	console.log('postCreate USER: ', user);
	//Retrieved all form fields
	const {
		name,
		type,
		size,
		flavour,
		filling,
		icing,
		topping,
		imgURl,
		date,
	} = req.body;
	// If img is not upload mantain original Url
	if (req.file !== undefined) {
		const { path } = req.file;
		console.log('postCreate PATH: ', path);
		if (imgURl === '') {
			res.render('cake/create', { message: 'No seleccionaste ninguna imagen' });
		} else if (name === '') {
			res.render('cake/create', {
				message: 'Ingresa un nombre para el pedido',
			});
		} else {
			//Calculate total cost using parameters
			let cost = calculateTotalCost(
				type,
				size,
				flavour,
				filling,
				icing,
				topping
			);
			const newCake = getRepository(Cake).create({
				name,
				type,
				size,
				flavour,
				filling,
				icing,
				topping,
				imgUrl: path,
				date,
				cost,
			});

			await getRepository(Cake)
				.save(newCake)
				.then(() => {
					console.log('postCreate NEWCAKE SAVED: ', newCake);
				})
				.catch((error) => console.log('saveCake error: ', error));

			const newOrder = getRepository(Order).create({
				user: userID,
				cake: newCake,
			});
			console.log('postCreate NEWORDER: ', newOrder);

			await getRepository(Order)
				.save(newOrder)
				.then(() => {
					console.log('postCreate NEWORDER SAVED: ', newOrder);
				})
				.catch((error) => console.log(error));
		}
		res.redirect('/user/profile');
	} else {
		res.render('cake/create', { message: 'No seleccionaste una imagen' });
	}
};

/**
 * Function to display details of cake
 * @param req Any
 * @param res Response
 * @param next NextFunction
 */
export const getCakeDetails = async (
	req: any,
	res: Response,
	next: NextFunction
) => {
	const { user } = req;
	const { cakeId } = req.params;
	const orderValidation = await getRepository(Order).findOne({ cake: cakeId });
	try {
		//Do validation if owner and display diferent options
		await getRepository(Cake)
			.findOne(cakeId)
			.then((cake) => {
				if (user['role'] === 'owner') {
					const owner = true;
					console.log('getCakeDetails SUCCCES: ', cake);
					res.render('cake/details', { cake, owner });
				} else {
					console.log('Status:', orderValidation.status);
					if (orderValidation.status !== 'Pendiente') {
						const bakingAlready = true;
						res.render('cake/details', { cake, bakingAlready });
					} else {
						console.log('getCakeDetails SUCCCES: ', cake);
						res.render('cake/details', { cake });
					}
				}
			})
			.catch((error) => {
				console.log('getCakeDetails ERROR: ', error);
			});
	} catch (error) {
		console.log('getCakeDetails ERROR: ', error);
	}
};

/**
 * Function to update cake
 * @param req any
 * @param res response
 * @param next nextfunction
 */
export const postCakeUpdate = async (
	req: any,
	res: Response,
	next: NextFunction
) => {
	try {
		const { cakeId } = req.params;
		const { user } = req;
		const userID = user['id'];
		const cakeToUpdate = await getRepository(Cake).findOne(cakeId);
		const orderToUpdate = await getRepository(Order).findOne({ cake: cakeId });
		console.log('postCakeUpdate CAKETOUPDATE: ', cakeToUpdate);
		console.log('postCakeUpdate ORDERTOUPDATE: ', orderToUpdate);
		let defaultName = '';
		let defaultImgUrl = '';
		if (req.file !== undefined) {
			const { path } = req.file;
			defaultImgUrl = path;
		} else {
			defaultImgUrl = cakeToUpdate.imgUrl;
		}
		const {
			name,
			type,
			size,
			flavour,
			filling,
			icing,
			topping,
			imgUrl,
			date,
			status,
		} = req.body;
		name === '' ? (defaultName = 'Mi pedido') : (defaultName = name);
		console.log('IMGURL: ', imgUrl);
		let newCost = calculateTotalCost(
			type,
			size,
			flavour,
			filling,
			icing,
			topping
		);
		await getRepository(Cake).update(cakeId, {
			name: defaultName,
			imgUrl: defaultImgUrl,
			type,
			size,
			flavour,
			filling,
			icing,
			topping,
			date,
			cost: newCost,
		});
		if (status) {
			console.log('Cambiando a: ', status);
			await getRepository(Order).update(orderToUpdate.id, {
				status,
			});
		}
		res.redirect('/user/profile');
	} catch (error) {
		console.log(error);
	}
};

/**
 * Function to delete cake
 * @param req 
 * @param res 
 * @param next 
 */
export const getCakeDelete = (
	req: Request,
	res: Response,
	next: NextFunction
) => {
	try {
		const { cakeId } = req.params;
		console.log('getCakeDelete CAKEID: ', cakeId);
		getRepository(Cake)
			.delete(cakeId)
			.then((del) => console.log('Cake deleted: ', del))
			.catch((error) => {
				console.log('getCakeDelete ERROR:', error);
			});
		res.redirect('/user/profile');
	} catch (error) {
		console.log('getCakeDelete ERROR:', error);
	}
};

/**
 * Function to calculate cost of type
 * @param type type
 */
const calculateCostType = (type: string): number => {
	if (type === 'Circular') return 50;
	if (type === 'Rectangular') return 60;
};

/**
 * Function to calculate cost of size
 * @param size size
 */
const calculateCostSize = (size: string): number => {
	if (size === 'Chico') return 80;
	if (size === 'Mediano') return 160;
	if (size === 'Grande') return 320;
};

/**
 * Function to calculate cost of flavour
 * @param flavour flavour
 */
const calculateCostFlavour = (flavour: string): number => {
	if (flavour === 'Chocolate') return 40;
	if (flavour === 'Nuez') return 30;
	if (flavour === 'Naranja') return 35;
	if (flavour === 'Coco') return 30;
	if (flavour === 'Vainilla') return 25;
	if (flavour === 'TresLeches') return 40;
	if (flavour === 'Envinado') return 80;
	if (flavour === 'Seco') return 0;
};

/**
 * Function to calculate cost of filling
 * @param filling filling
 */
const calculateCostFilling = (filling: string): number => {
	if (filling === 'MezclaDeFrutas') return 30;
	if (filling === 'Pinia') return 25;
	if (filling === 'Mango') return 30;
	if (filling === 'Fresa') return 35;
	if (filling === 'Durazno') return 35;
	if (filling === 'Pera') return 30;
	if (filling === 'NuezPicada') return 35;
	if (filling === 'Chantilly') return 35;
	if (filling === 'CremaPastelera') return 25;
	if (filling === 'Mermelada') return 40;
	if (filling === 'Nutella') return 60;
	if (filling === 'Ninguno') return 0;
};

/**
 * Function to calculate icing cost
 * @param icing icing
 */
const calculateCostIcing = (icing: string): number => {
	if (icing === 'Vainilla') return 20;
	if (icing === 'Chocolate') return 25;
};


/**
 * Function to calculare cost of topping
 * @param topping topping
 */
const calculateCostTopping = (topping: string): number => {
	if (topping === 'Frase') return 20;
	if (topping === 'Fondant') return 40;
	if (topping === 'Chantilly') return 25;
	if (topping === 'Oblea') return 20;
};

/**
 * Function to return the total cost of cake
 * @param type 
 * @param size 
 * @param flavour 
 * @param filling 
 * @param icing 
 * @param topping 
 */
const calculateTotalCost = (
	type: string,
	size: string,
	flavour: string,
	filling: string,
	icing: string,
	topping: string
): number => {
	let total = 0;
	total += calculateCostType(type);
	total += calculateCostSize(size);
	total += calculateCostFlavour(flavour);
	total += calculateCostFilling(filling);
	total += calculateCostIcing(icing);
	total += calculateCostTopping(topping);
	return total;
};
