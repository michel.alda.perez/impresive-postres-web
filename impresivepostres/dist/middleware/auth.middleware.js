"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authGlobal = exports.isActive = exports.isLoggedIn = void 0;
exports.isLoggedIn = function (req, res, next) {
    req.isAuthenticated() ? next() : res.redirect('/auth/login');
};
exports.isActive = function (req, res, next) {
    req.user.status
        ? next()
        : res.render('auth/login', { message: 'Verifica tu cuenta' });
};
exports.authGlobal = function (req, res, next) {
    if (req.isAuthenticated()) {
        req.app.locals.logged = true;
    }
    else {
        req.app.locals.logged = false;
    }
    next();
};
