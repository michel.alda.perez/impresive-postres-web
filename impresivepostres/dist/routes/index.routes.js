"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var router = express_1.Router();
router.get('/', function (req, res, next) {
    res.render('index');
});
router.get('/about', function (req, res, next) {
    res.render('about');
});
exports.default = router;
