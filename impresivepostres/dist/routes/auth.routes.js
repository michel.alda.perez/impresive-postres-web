"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var passport_1 = __importDefault(require("passport"));
var auth_controller_1 = require("../controllers/auth.controller");
var auth_middleware_1 = require("../middleware/auth.middleware");
var router = express_1.Router();
router.get('/signup', auth_controller_1.getSignup);
router.post('/signup', auth_controller_1.postSignup);
router.get('/login', auth_controller_1.getLogin);
router.post('/login', auth_controller_1.postLogin);
router.get('/logout', auth_middleware_1.isLoggedIn, auth_middleware_1.isActive, auth_controller_1.logout);
router.get('/confirm/:confirmationCode', auth_controller_1.getConfirm);
router.get('/google', passport_1.default.authenticate('google', {
    scope: [
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email',
    ],
}));
router.get('/google/callback', passport_1.default.authenticate('google', {
    successRedirect: '/user/profile',
    failureRedirect: '/auth/login',
}));
router.get('/facebook', passport_1.default.authenticate('facebook', {
    scope: ['email'],
}));
router.get('/facebook/callback', passport_1.default.authenticate('facebook', {
    successRedirect: '/user/profile',
    failureRedirect: '/auth/login',
}));
exports.default = router;
