"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var cloudinary_config_1 = __importDefault(require("../config/cloudinary.config"));
var cake_controller_1 = require("../controllers/cake.controller");
var auth_middleware_1 = require("../middleware/auth.middleware");
var router = express_1.Router();
router.get('/create', auth_middleware_1.isLoggedIn, auth_middleware_1.isActive, cake_controller_1.getCreate);
router.post('/create', auth_middleware_1.isLoggedIn, auth_middleware_1.isActive, cloudinary_config_1.default.single('imgUrl'), cake_controller_1.postCreate);
router.get('/:cakeId/details', auth_middleware_1.isLoggedIn, auth_middleware_1.isActive, cake_controller_1.getCakeDetails);
router.post('/:cakeId/update', auth_middleware_1.isLoggedIn, auth_middleware_1.isActive, cloudinary_config_1.default.single('imgUrl'), cake_controller_1.postCakeUpdate);
router.get('/:cakeId/delete', auth_middleware_1.isLoggedIn, auth_middleware_1.isActive, cake_controller_1.getCakeDelete);
exports.default = router;
