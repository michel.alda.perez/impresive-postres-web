"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var cloudinary_config_1 = __importDefault(require("../config/cloudinary.config"));
var auth_middleware_1 = require("../middleware/auth.middleware");
var user_controller_1 = require("../controllers/user.controller");
var router = express_1.Router();
router.get('/profile', auth_middleware_1.isLoggedIn, auth_middleware_1.isActive, user_controller_1.getProfile);
router.post('/:userId/update', auth_middleware_1.isLoggedIn, auth_middleware_1.isActive, cloudinary_config_1.default.single('imgUrl'), user_controller_1.postProfile);
exports.default = router;
