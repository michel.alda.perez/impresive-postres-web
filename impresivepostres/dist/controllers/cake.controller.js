"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCakeDelete = exports.postCakeUpdate = exports.getCakeDetails = exports.postCreate = exports.getCreate = void 0;
var typeorm_1 = require("typeorm");
var Order_entity_1 = require("../entities/Order.entity");
var Cake_entity_1 = require("../entities/Cake.entity");
exports.getCreate = function (req, res, next) {
    res.render('cake/create');
};
exports.postCreate = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user, userID, _a, name, type, size, flavour, filling, icing, topping, imgURl, date, path, cost, newCake_1, newOrder_1;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                user = req.user;
                userID = user['id'];
                console.log('postCreate USER: ', user);
                _a = req.body, name = _a.name, type = _a.type, size = _a.size, flavour = _a.flavour, filling = _a.filling, icing = _a.icing, topping = _a.topping, imgURl = _a.imgURl, date = _a.date;
                if (!(req.file !== undefined)) return [3 /*break*/, 6];
                path = req.file.path;
                console.log('postCreate PATH: ', path);
                if (!(imgURl === '')) return [3 /*break*/, 1];
                res.render('cake/create', { message: 'No seleccionaste ninguna imagen' });
                return [3 /*break*/, 5];
            case 1:
                if (!(name === '')) return [3 /*break*/, 2];
                res.render('cake/create', {
                    message: 'Ingresa un nombre para el pedido',
                });
                return [3 /*break*/, 5];
            case 2:
                cost = calculateTotalCost(type, size, flavour, filling, icing, topping);
                newCake_1 = typeorm_1.getRepository(Cake_entity_1.Cake).create({
                    name: name,
                    type: type,
                    size: size,
                    flavour: flavour,
                    filling: filling,
                    icing: icing,
                    topping: topping,
                    imgUrl: path,
                    date: date,
                    cost: cost,
                });
                return [4 /*yield*/, typeorm_1.getRepository(Cake_entity_1.Cake)
                        .save(newCake_1)
                        .then(function () {
                        console.log('postCreate NEWCAKE SAVED: ', newCake_1);
                    })
                        .catch(function (error) { return console.log('saveCake error: ', error); })];
            case 3:
                _b.sent();
                newOrder_1 = typeorm_1.getRepository(Order_entity_1.Order).create({
                    user: userID,
                    cake: newCake_1,
                });
                console.log('postCreate NEWORDER: ', newOrder_1);
                return [4 /*yield*/, typeorm_1.getRepository(Order_entity_1.Order)
                        .save(newOrder_1)
                        .then(function () {
                        console.log('postCreate NEWORDER SAVED: ', newOrder_1);
                    })
                        .catch(function (error) { return console.log(error); })];
            case 4:
                _b.sent();
                _b.label = 5;
            case 5:
                res.redirect('/user/profile');
                return [3 /*break*/, 7];
            case 6:
                res.render('cake/create', { message: 'No seleccionaste una imagen' });
                _b.label = 7;
            case 7: return [2 /*return*/];
        }
    });
}); };
exports.getCakeDetails = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user, cakeId, orderValidation, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                user = req.user;
                cakeId = req.params.cakeId;
                return [4 /*yield*/, typeorm_1.getRepository(Order_entity_1.Order).findOne({ cake: cakeId })];
            case 1:
                orderValidation = _a.sent();
                _a.label = 2;
            case 2:
                _a.trys.push([2, 4, , 5]);
                return [4 /*yield*/, typeorm_1.getRepository(Cake_entity_1.Cake)
                        .findOne(cakeId)
                        .then(function (cake) {
                        if (user['role'] === 'owner') {
                            var owner = true;
                            console.log('getCakeDetails SUCCCES: ', cake);
                            res.render('cake/details', { cake: cake, owner: owner });
                        }
                        else {
                            console.log('Status:', orderValidation.status);
                            if (orderValidation.status !== 'Pendiente') {
                                var bakingAlready = true;
                                res.render('cake/details', { cake: cake, bakingAlready: bakingAlready });
                            }
                            else {
                                console.log('getCakeDetails SUCCCES: ', cake);
                                res.render('cake/details', { cake: cake });
                            }
                        }
                    })
                        .catch(function (error) {
                        console.log('getCakeDetails ERROR: ', error);
                    })];
            case 3:
                _a.sent();
                return [3 /*break*/, 5];
            case 4:
                error_1 = _a.sent();
                console.log('getCakeDetails ERROR: ', error_1);
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); };
exports.postCakeUpdate = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var cakeId, user, userID, cakeToUpdate, orderToUpdate, defaultName, defaultImgUrl, path, _a, name_1, type, size, flavour, filling, icing, topping, imgUrl, date, status_1, newCost, error_2;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _b.trys.push([0, 6, , 7]);
                cakeId = req.params.cakeId;
                user = req.user;
                userID = user['id'];
                return [4 /*yield*/, typeorm_1.getRepository(Cake_entity_1.Cake).findOne(cakeId)];
            case 1:
                cakeToUpdate = _b.sent();
                return [4 /*yield*/, typeorm_1.getRepository(Order_entity_1.Order).findOne({ cake: cakeId })];
            case 2:
                orderToUpdate = _b.sent();
                console.log('postCakeUpdate CAKETOUPDATE: ', cakeToUpdate);
                console.log('postCakeUpdate ORDERTOUPDATE: ', orderToUpdate);
                defaultName = '';
                defaultImgUrl = '';
                if (req.file !== undefined) {
                    path = req.file.path;
                    defaultImgUrl = path;
                }
                else {
                    defaultImgUrl = cakeToUpdate.imgUrl;
                }
                _a = req.body, name_1 = _a.name, type = _a.type, size = _a.size, flavour = _a.flavour, filling = _a.filling, icing = _a.icing, topping = _a.topping, imgUrl = _a.imgUrl, date = _a.date, status_1 = _a.status;
                name_1 === '' ? (defaultName = 'Mi pedido') : (defaultName = name_1);
                console.log('IMGURL: ', imgUrl);
                newCost = calculateTotalCost(type, size, flavour, filling, icing, topping);
                return [4 /*yield*/, typeorm_1.getRepository(Cake_entity_1.Cake).update(cakeId, {
                        name: defaultName,
                        imgUrl: defaultImgUrl,
                        type: type,
                        size: size,
                        flavour: flavour,
                        filling: filling,
                        icing: icing,
                        topping: topping,
                        date: date,
                        cost: newCost,
                    })];
            case 3:
                _b.sent();
                if (!status_1) return [3 /*break*/, 5];
                console.log('Cambiando a: ', status_1);
                return [4 /*yield*/, typeorm_1.getRepository(Order_entity_1.Order).update(orderToUpdate.id, {
                        status: status_1,
                    })];
            case 4:
                _b.sent();
                _b.label = 5;
            case 5:
                res.redirect('/user/profile');
                return [3 /*break*/, 7];
            case 6:
                error_2 = _b.sent();
                console.log(error_2);
                return [3 /*break*/, 7];
            case 7: return [2 /*return*/];
        }
    });
}); };
exports.getCakeDelete = function (req, res, next) {
    try {
        var cakeId = req.params.cakeId;
        console.log('getCakeDelete CAKEID: ', cakeId);
        typeorm_1.getRepository(Cake_entity_1.Cake)
            .delete(cakeId)
            .then(function (del) { return console.log('Cake deleted: ', del); })
            .catch(function (error) {
            console.log('getCakeDelete ERROR:', error);
        });
        res.redirect('/user/profile');
    }
    catch (error) {
        console.log('getCakeDelete ERROR:', error);
    }
};
var calculateCostType = function (type) {
    if (type === 'Circular')
        return 50;
    if (type === 'Rectangular')
        return 60;
};
var calculateCostSize = function (size) {
    if (size === 'Chico')
        return 80;
    if (size === 'Mediano')
        return 160;
    if (size === 'Grande')
        return 320;
};
var calculateCostFlavour = function (flavour) {
    if (flavour === 'Chocolate')
        return 40;
    if (flavour === 'Nuez')
        return 30;
    if (flavour === 'Naranja')
        return 35;
    if (flavour === 'Coco')
        return 30;
    if (flavour === 'Vainilla')
        return 25;
    if (flavour === 'TresLeches')
        return 40;
    if (flavour === 'Envinado')
        return 80;
    if (flavour === 'Seco')
        return 0;
};
var calculateCostFilling = function (filling) {
    if (filling === 'MezclaDeFrutas')
        return 30;
    if (filling === 'Pinia')
        return 25;
    if (filling === 'Mango')
        return 30;
    if (filling === 'Fresa')
        return 35;
    if (filling === 'Durazno')
        return 35;
    if (filling === 'Pera')
        return 30;
    if (filling === 'NuezPicada')
        return 35;
    if (filling === 'Chantilly')
        return 35;
    if (filling === 'CremaPastelera')
        return 25;
    if (filling === 'Mermelada')
        return 40;
    if (filling === 'Nutella')
        return 60;
    if (filling === 'Ninguno')
        return 0;
};
var calculateCostIcing = function (icing) {
    if (icing === 'Vainilla')
        return 20;
    if (icing === 'Chocolate')
        return 25;
};
var calculateCostTopping = function (topping) {
    if (topping === 'Frase')
        return 20;
    if (topping === 'Fondant')
        return 40;
    if (topping === 'Chantilly')
        return 25;
    if (topping === 'Oblea')
        return 20;
};
var calculateTotalCost = function (type, size, flavour, filling, icing, topping) {
    var total = 0;
    total += calculateCostType(type);
    total += calculateCostSize(size);
    total += calculateCostFlavour(flavour);
    total += calculateCostFilling(filling);
    total += calculateCostIcing(icing);
    total += calculateCostTopping(topping);
    return total;
};
