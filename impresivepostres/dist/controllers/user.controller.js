"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.postProfile = exports.getProfile = void 0;
var Cake_entity_1 = require("../entities/Cake.entity");
var typeorm_1 = require("typeorm");
var User_entity_1 = require("../entities/User.entity");
exports.getProfile = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user, userID, owner, cakes, error_1, cakes, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                user = req.user;
                userID = user['id'];
                console.log('getProfile USER: ', user);
                console.log('getProfile USERID: ', userID);
                if (!(user['role'] === 'owner')) return [3 /*break*/, 5];
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                owner = true;
                return [4 /*yield*/, typeorm_1.getRepository(Cake_entity_1.Cake)
                        .createQueryBuilder('cake')
                        .innerJoinAndSelect('cake.order', 'order')
                        .execute()];
            case 2:
                cakes = _a.sent();
                console.log('getProfile  ADMIN USERCAKES: ', cakes);
                res.render('user/profile', { user: user, cakes: cakes, owner: owner });
                return [3 /*break*/, 4];
            case 3:
                error_1 = _a.sent();
                console.log(error_1);
                return [3 /*break*/, 4];
            case 4: return [3 /*break*/, 8];
            case 5:
                _a.trys.push([5, 7, , 8]);
                return [4 /*yield*/, typeorm_1.getRepository(Cake_entity_1.Cake)
                        .createQueryBuilder('cake')
                        .innerJoinAndSelect('cake.order', 'order')
                        .where('order.user_id = :user_id', { user_id: userID })
                        .execute()];
            case 6:
                cakes = _a.sent();
                console.log('getProfile USERCAKES: ', cakes);
                res.render('user/profile', { user: user, cakes: cakes });
                return [3 /*break*/, 8];
            case 7:
                error_2 = _a.sent();
                console.log(error_2);
                return [3 /*break*/, 8];
            case 8: return [2 /*return*/];
        }
    });
}); };
exports.postProfile = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, path, user, error_3, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 7, , 8]);
                id = req.user.id;
                if (!(req.file !== undefined)) return [3 /*break*/, 5];
                path = req.file.path;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).update(id, {
                        imgUrl: path,
                    })];
            case 2:
                user = _a.sent();
                req.user = user;
                res.redirect('/user/profile');
                return [3 /*break*/, 4];
            case 3:
                error_3 = _a.sent();
                console.log('Image not selected');
                res.render('user/profile', { message: 'No seleccionaste una imagen' });
                return [3 /*break*/, 4];
            case 4: return [3 /*break*/, 6];
            case 5:
                res.redirect('/user/profile');
                _a.label = 6;
            case 6: return [3 /*break*/, 8];
            case 7:
                error_4 = _a.sent();
                console.log('not path');
                res.redirect('/auth/profile');
                return [3 /*break*/, 8];
            case 8: return [2 /*return*/];
        }
    });
}); };
