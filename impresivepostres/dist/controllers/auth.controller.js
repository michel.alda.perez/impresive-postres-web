"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getConfirm = exports.logout = exports.postLogin = exports.getLogin = exports.postSignup = exports.getSignup = void 0;
var User_entity_1 = require("../entities/User.entity");
var typeorm_1 = require("typeorm");
var passport_config_1 = __importDefault(require("../config/passport.config"));
var bcrypt_1 = __importDefault(require("bcrypt"));
var nodemailer_config_1 = require("../config/nodemailer.config");
//SIGNUP
exports.getSignup = function (req, res, next) {
    res.render('auth/signup');
};
exports.postSignup = function (req, res, next) {
    var _a = req.body, email = _a.email, username = _a.username, password = _a.password, checkpass = _a.checkpass;
    var user = {
        username: username,
        email: email,
        password: password,
        checkpass: checkpass,
    };
    if (email === '' || password === '' || username === '') {
        res.render('auth/signup', {
            message: 'Faltan campos por llenar',
            user: user,
        });
    }
    else if (!validateEmail(email)) {
        res.render('auth/signup', {
            message: 'Dirección de correo no válida',
            user: user,
        });
    }
    else if (!validatePassword(password)) {
        res.render('auth/signup', {
            message: 'La contraseña debe tener al menos 8 caracteres, una letra minúscula y una mayúscula',
            user: user,
        });
    }
    else if (checkpass !== password) {
        res.render('auth/signup', {
            message: 'Las contraseñas no coinciden',
            user: user,
        });
    }
    else {
        var randomCode_1 = generateRandomToken();
        typeorm_1.getRepository(User_entity_1.User)
            .findOne({ email: email })
            .then(function (user) {
            if (!user) {
                var salt = bcrypt_1.default.genSaltSync(10);
                var hashPassword = bcrypt_1.default.hashSync(password, salt);
                var newUser = typeorm_1.getRepository(User_entity_1.User).create({
                    email: email,
                    username: username,
                    password: hashPassword,
                    confirmationCode: randomCode_1,
                });
                typeorm_1.getRepository(User_entity_1.User)
                    .save(newUser)
                    .then(function (usr) { return __awaiter(void 0, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, nodemailer_config_1.confirmAccount(email, process.env.ENDPOINT + "/auth/confirm/" + usr.confirmationCode)];
                            case 1:
                                _a.sent();
                                res.redirect('/auth/login');
                                return [2 /*return*/];
                        }
                    });
                }); })
                    .catch(function (err) { return console.log(err); });
            }
            else {
                res.render('auth/signup', {
                    message: 'Esta dirección de correo ya esta en uso',
                });
            }
        })
            .catch(function (err) { return console.log(err); });
    }
};
//LOGIN
exports.getLogin = function (req, res, next) {
    var message = req.flash('error')[0];
    res.render('auth/login', { message: message });
};
exports.postLogin = passport_config_1.default.authenticate('local', {
    successRedirect: '/user/profile',
    failureRedirect: '/auth/login',
    failureFlash: true,
});
//LOGOUT
exports.logout = function (req, res, next) {
    req.session.destroy(function () {
        req.logOut();
        res.clearCookie('graphNodeCookie');
        res.status(200);
        res.redirect('/auth/login');
    });
};
exports.getConfirm = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var confirmationCode, userConfirm, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                confirmationCode = req.params.confirmationCode;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 4, , 5]);
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne({
                        confirmationCode: confirmationCode,
                    })];
            case 2:
                userConfirm = _a.sent();
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).update(userConfirm.id, { status: true })];
            case 3:
                _a.sent();
                res.redirect('/user/profile');
                return [3 /*break*/, 5];
            case 4:
                error_1 = _a.sent();
                console.log('getConfirm error: ', error_1);
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); };
//AUXILIAR FUNCTIONS
var validateEmail = function (email) {
    var emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return email.match(emailFormat) ? true : false;
};
var validatePassword = function (password) {
    var passwordFormat = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;
    return password.match(passwordFormat) ? true : false;
};
var generateRandomToken = function () {
    var characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var token = '';
    for (var i = 0; i < 25; i++) {
        token += characters[Math.floor(Math.random() * characters.length)];
    }
    return token;
};
