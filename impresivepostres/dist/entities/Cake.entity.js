"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cake = void 0;
var typeorm_1 = require("typeorm");
var Order_entity_1 = require("./Order.entity");
var Cake = /** @class */ (function () {
    function Cake() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", String)
    ], Cake.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column({ default: 'Mi pastel' }),
        __metadata("design:type", String)
    ], Cake.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column({
            type: 'enum',
            enum: ['Circular', 'Rectangular'],
            default: 'Circular',
        }),
        __metadata("design:type", String)
    ], Cake.prototype, "type", void 0);
    __decorate([
        typeorm_1.Column({
            type: 'enum',
            enum: ['Chico', 'Mediano', 'Grande'],
            default: 'Chico',
        }),
        __metadata("design:type", String)
    ], Cake.prototype, "size", void 0);
    __decorate([
        typeorm_1.Column({
            type: 'enum',
            enum: [
                'Chocolate',
                'Nuez',
                'Naranja',
                'Coco',
                'Vainilla',
                'TresLeches',
                'Envinado',
                'Seco',
            ],
            default: 'Seco',
        }),
        __metadata("design:type", String)
    ], Cake.prototype, "flavour", void 0);
    __decorate([
        typeorm_1.Column({
            type: 'enum',
            enum: [
                'MezclaDeFrutas',
                'Pinia',
                'Mango',
                'Fresa',
                'Durazno',
                'Pera',
                'NuezPicada',
                'Chantilly',
                'CremaPastelera',
                'Mermelada',
                'Nutella',
                'Ninguno',
            ],
            default: 'Chantilly',
        }),
        __metadata("design:type", String)
    ], Cake.prototype, "filling", void 0);
    __decorate([
        typeorm_1.Column({
            type: 'enum',
            enum: ['Vainilla', 'Chocolate'],
            default: 'Vainilla',
        }),
        __metadata("design:type", String)
    ], Cake.prototype, "icing", void 0);
    __decorate([
        typeorm_1.Column({
            type: 'enum',
            enum: ['Frase', 'Fondant', 'Chantilly', 'Oblea'],
            default: 'Oblea',
        }),
        __metadata("design:type", String)
    ], Cake.prototype, "topping", void 0);
    __decorate([
        typeorm_1.Column({
            default: 'https://res.cloudinary.com/dbc1hdxqi/image/upload/v1591994110/impresivePostresWeb/impresive-postres-logo_kole6d.png',
        }),
        __metadata("design:type", String)
    ], Cake.prototype, "imgUrl", void 0);
    __decorate([
        typeorm_1.Column({ default: 200 }),
        __metadata("design:type", Number)
    ], Cake.prototype, "cost", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Date)
    ], Cake.prototype, "date", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Order_entity_1.Order; }, function (order) { return order.cake; }),
        typeorm_1.JoinColumn({ name: 'order_id' }),
        __metadata("design:type", Array)
    ], Cake.prototype, "order", void 0);
    Cake = __decorate([
        typeorm_1.Entity()
    ], Cake);
    return Cake;
}());
exports.Cake = Cake;
